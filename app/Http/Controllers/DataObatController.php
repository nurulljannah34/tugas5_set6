<?php

namespace App\Http\Controllers;

use App\Models\data_obat;
use Illuminate\Http\Request;

class DataObatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $obat = $request->all();
        $Obat = data_obat::create($obat);

        return response()->json($Obat);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\data_obat  $data_obat
     * @return \Illuminate\Http\Response
     */
    public function show(data_obat $data_obat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\data_obat  $data_obat
     * @return \Illuminate\Http\Response
     */
    public function edit(data_obat $data_obat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\data_obat  $data_obat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, data_obat $data_obat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\data_obat  $data_obat
     * @return \Illuminate\Http\Response
     */
    public function destroy(data_obat $data_obat)
    {
        //
    }
}

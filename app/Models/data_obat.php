<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class data_obat extends Model
{
    protected $table = 'data_obats';
    protected $fillable = [
        'namaobat','kegunaanpadaobat','kebutuhanpadaobat','datapengeluaranobat'
    ];
}
